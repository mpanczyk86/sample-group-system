package com.mp.learn.gs.business.api.ex;

/**
 * A {@link GroupBusinessLogicContingencyException contingency} thrown when a user tries to perform
 * an action with insufficient access rights. 
 */
public class UserNotAuthorizedException extends GroupBusinessLogicContingencyException {

  private static final long serialVersionUID = -3171360173621685077L;
  
  public UserNotAuthorizedException(String messageTemplate, Object... templateArguments) {
    super(String.format("not authorized to %s", messageTemplate), templateArguments);
  }
}
