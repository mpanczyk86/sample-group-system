package com.mp.learn.gs.business.impl;

import com.google.common.collect.Iterables;
import com.mp.learn.gs.business.api.service.UserManager;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.api.value.UserInfo;
import com.mp.learn.gs.business.impl.acl.UserAuthorizer;
import com.mp.learn.gs.business.impl.acl.UserAuthorizerFactory;
import com.mp.learn.gs.business.impl.asm.UserInfoAssembler;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.service.Transactor;
import com.mp.learn.gs.business.spi.service.UserDao;
import com.mp.learn.gs.business.spi.value.GroupCriteria;
import com.mp.learn.gs.business.spi.value.GroupState;

public class UserManagerImpl implements UserManager {

  private final UserId userId;
  
  private final UserDao userDao;
  private final UserInfoAssembler userInfoAssembler;
  private final GroupDao groupDao;
  private final UserAuthorizer currentUserAuthorizer;
  private final Transactor transactor;

  public UserManagerImpl(
      UserId userId,
      UserDao userDao,
      GroupDao groupDao,
      Transactor transactor,
      UserAuthorizerFactory userAuthorizerFactory,
      UserInfoAssembler userInfoAssembler) {
    this.userId = userId;
    this.userDao = userDao;
    this.groupDao = groupDao;
    this.transactor = transactor;
    this.currentUserAuthorizer = userAuthorizerFactory.createForCurrentUser();
    this.userInfoAssembler = userInfoAssembler;
  }
  
  @Override
  public UserInfo show() {
    return userInfoAssembler.apply(userDao.load(userId));
  }

  @Override
  public void delete() {
    currentUserAuthorizer.checkCanDeleteUser(userId);
    transactor.transact(() -> {
      Iterable<GroupState> containingGroups =
          groupDao.query(GroupCriteria.any().thatContainsMember(userId));
      Iterable<GroupState> modifiedGroups =
          Iterables.transform(containingGroups, state -> withoutMember(state, userId));
      groupDao.saveBatch(modifiedGroups);
      userDao.delete(userId);
    });
  }

  private GroupState withoutMember(GroupState state, UserId userId) {
    return state.withoutMembership(state.getPotentialMembershipOf(userId).get());
  }
}
