package com.mp.learn.gs.business.api.service;

import com.mp.learn.gs.business.api.ex.UserAlreadyInGroupException;
import com.mp.learn.gs.business.api.ex.UserNotInGroupException;
import com.mp.learn.gs.business.api.value.GroupInfo;

/**
 * A scoped manager of a single Group.
 */
public interface GroupManager {

  /**
   * Returns the Group's details.
   */
	GroupInfo show();
	
	/**
	 * Increases the size limit of the Group by 1.
	 */
	void increaseLimit();
	
	/**
	 * Enlists the current user within the Group.
	 * <p>
	 * Throws a {@link UserAlreadyInGroupException} iff the current user was already a member of the
	 * Group.
	 */
	void join();
	
	/**
	 * Removes the current user from the Group.
	 * <p>
   * Throws a {@link UserNotInGroupException} iff the current user was not a member of the Group.
	 */
	void leave();
}
