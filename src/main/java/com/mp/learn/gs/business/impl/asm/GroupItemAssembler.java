package com.mp.learn.gs.business.impl.asm;

import com.mp.learn.gs.business.api.value.GroupItem;
import com.mp.learn.gs.business.spi.value.GroupState;

public class GroupItemAssembler {

  public GroupItem apply(GroupState state) {
    return GroupItem.create(state.id(), state.name(), state.memberships().size());
  }
}
