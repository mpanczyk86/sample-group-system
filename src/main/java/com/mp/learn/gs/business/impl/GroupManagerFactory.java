package com.mp.learn.gs.business.impl;

import java.time.Clock;

import javax.inject.Inject;

import com.mp.learn.gs.business.api.service.GroupManager;
import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.impl.acl.CurrentUserService;
import com.mp.learn.gs.business.impl.asm.GroupInfoAssembler;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.service.Transactor;
import com.mp.learn.gs.business.spi.service.UserDao;

public class GroupManagerFactory {

  private final GroupDao groupDao;
  private final CurrentUserService currentUserService;
  private final UserDao userDao;
  private final Clock clock;
  private final GroupInfoAssembler groupInfoAssembler;
  private final Transactor transactor;

  @Inject
  public GroupManagerFactory(
      GroupDao groupDao,
      UserDao userDao,
      Transactor transactor,
      CurrentUserService currentUserService,
      Clock clock,
      GroupInfoAssembler groupInfoAssembler) {
    this.userDao = userDao;
    this.groupDao = groupDao;
    this.transactor = transactor;
    this.currentUserService = currentUserService;
    this.clock = clock;
    this.groupInfoAssembler = groupInfoAssembler;
  }
  
  public GroupManager create(GroupId groupId) {
    return new GroupManagerImpl(groupId, currentUserService, groupDao, userDao, transactor, clock,
        groupInfoAssembler);
  }
}