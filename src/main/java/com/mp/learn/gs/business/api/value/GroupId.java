package com.mp.learn.gs.business.api.value;

import java.util.UUID;

/**
 * An id of a Group.
 */
public class GroupId extends ExternalId {

	private GroupId(UUID uuid) {
		super(uuid);
	}

  /**
	 * Creates an instance from {@link UUID}.
	 */
	public static GroupId fromUuid(UUID uuid) {
		return new GroupId(uuid);
	}

	/**
	 * Creates a new, unique instance.
	 */
  public static GroupId createNew() {
    return fromUuid(UUID.randomUUID());
  }
}
