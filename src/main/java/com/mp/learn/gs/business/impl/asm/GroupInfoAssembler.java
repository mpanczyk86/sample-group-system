package com.mp.learn.gs.business.impl.asm;

import java.util.Map;

import javax.inject.Inject;

import com.mp.learn.gs.business.api.value.GroupInfo;
import com.mp.learn.gs.business.api.value.MemberItem;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.value.GroupState;
import com.mp.learn.gs.business.spi.value.UserState;

import autovalue.shaded.com.google.common.common.collect.Iterables;
import autovalue.shaded.com.google.common.common.collect.Maps;

public class GroupInfoAssembler {
  
  private final MemberItemAssembler memberItemAssembler;

  @Inject
  public GroupInfoAssembler(MemberItemAssembler memberItemAssembler) {
    this.memberItemAssembler = memberItemAssembler;
  }
  
  public GroupInfo apply(GroupState state, Iterable<UserState> memberStates) {
    Map<UserId, UserState> memberStatesByUserId = Maps.uniqueIndex(memberStates, UserState::id);
    
    Iterable<MemberItem> memberItems = Iterables.transform(state.memberships(), membership -> {
      UserState userState = memberStatesByUserId.get(membership.userId());
      return memberItemAssembler.apply(membership, userState);
    });
    
    return GroupInfo.create(state.name(), state.creationInstant(), state.limit(), memberItems);
  }
}
