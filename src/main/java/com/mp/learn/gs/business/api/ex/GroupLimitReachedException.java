package com.mp.learn.gs.business.api.ex;

/**
 * A {@link GroupBusinessLogicContingencyException contingency} thrown when a user tries to join a
 * group which is at its size limit. 
 */
public class GroupLimitReachedException extends GroupBusinessLogicContingencyException {

  private static final long serialVersionUID = 3682335936036957619L;
}
