package com.mp.learn.gs.business.impl;

import java.time.Clock;

import com.mp.learn.gs.business.api.ex.GroupLimitReachedException;
import com.mp.learn.gs.business.api.ex.UserAlreadyInGroupException;
import com.mp.learn.gs.business.api.ex.UserNotInGroupException;
import com.mp.learn.gs.business.api.service.GroupManager;
import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.api.value.GroupInfo;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.impl.acl.CurrentUserService;
import com.mp.learn.gs.business.impl.asm.GroupInfoAssembler;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.service.Transactor;
import com.mp.learn.gs.business.spi.service.UserDao;
import com.mp.learn.gs.business.spi.value.GroupState;
import com.mp.learn.gs.business.spi.value.MembershipState;
import com.mp.learn.gs.business.spi.value.UserState;

import autovalue.shaded.com.google.common.common.base.Optional;
import autovalue.shaded.com.google.common.common.collect.Iterables;

public class GroupManagerImpl implements GroupManager {

  private final GroupId groupId;

  private final GroupDao groupDao;
  private final UserDao userDao;
  private final CurrentUserService currentUserService;
  private final Clock clock;
  private final GroupInfoAssembler groupInfoAssembler;
  private final Transactor transactor;

  public GroupManagerImpl(
      GroupId groupId,
      CurrentUserService currentUserService,
      GroupDao groupDao,
      UserDao userDao,
      Transactor transactor,
      Clock clock,
      GroupInfoAssembler groupInfoAssembler) {
    this.groupId = groupId;
    this.currentUserService = currentUserService;
    this.groupDao = groupDao;
    this.userDao = userDao;
    this.transactor = transactor;
    this.clock = clock;
    this.groupInfoAssembler = groupInfoAssembler;
  }

  @Override
  public GroupInfo show() {
    return transactor.transact(() -> {
      GroupState groupState = groupDao.load(groupId);
      Iterable<UserState> memberStates = userDao.loadBatch(
          Iterables.transform(groupState.memberships(), MembershipState::userId));
      return groupInfoAssembler.apply(groupState, memberStates);
    });
  }

  @Override
  public void increaseLimit() {
    transactor.transact(() -> {
      GroupState state = groupDao.load(groupId);
      groupDao.save(state.withLimit(state.limit() + 1));
    });
  }
  
  @Override
  public void join() {
    transactor.transact(() -> {
      GroupState state = groupDao.load(groupId);
      groupDao.save(withNewMember(state, currentUserService.getCurrentUserId()));
    });
  }

  @Override
  public void leave() {
    transactor.transact(() -> {
      GroupState state = groupDao.load(groupId);
      groupDao.save(withoutExistingMember(state, currentUserService.getDeclaredCurrentUserId()));
    });
  }
  
  private GroupState withoutExistingMember(GroupState groupState, UserId userId) {
    MembershipState existingMembership = getMembershipOfUser(groupState, userId);
    return groupState.withoutMembership(existingMembership);
  }
  
  private MembershipState getMembershipOfUser(GroupState groupState, UserId userId) {
    Optional<MembershipState> result = groupState.getPotentialMembershipOf(userId);
    if (!result.isPresent()) {
      throw new UserNotInGroupException();      
    }
    return result.get();
  }
  
  private GroupState withNewMember(GroupState groupState, UserId userId) {
    checkIsNotMember(groupState, userId);
    checkIsBelowLimit(groupState);
    MembershipState newMembership = MembershipState.create(userId, clock.instant());
    return groupState.withMembership(newMembership);
  }
  
  private void checkIsNotMember(GroupState groupState, UserId userId) {
    if (groupState.getPotentialMembershipOf(userId).isPresent()) {
      throw new UserAlreadyInGroupException();
    }
  }
  
  private void checkIsBelowLimit(GroupState groupState) {
    if (groupState.memberships().size() == groupState.limit()) {
      throw new GroupLimitReachedException();
    }
  }
}
