package com.mp.learn.gs.business.spi.service;

/**
 * A DAO transaction keeper.
 */
public interface Transactor {

  /**
   * A transactional unit of work that delivers a result.
   */
  public interface Work<T> {
    
    /**
     * Returns a result of this work unit.
     */
    T perform();
  }
  
  /**
   * A transactional unit of work that does not deliver any result.
   */
  public interface VoidWork {
    
    /**
     * Performs this work unit.
     */
    void perform();
  }
  
  /**
   * Returns a result of the given work unit performed within a single transaction.
   */
  <T> T transact(Work<T> work);
  
  /**
   * Performs the given work unit within a single transaction.
   */
  void transact(VoidWork voidWork);
}
