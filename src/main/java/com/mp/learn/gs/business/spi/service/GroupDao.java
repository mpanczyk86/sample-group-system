package com.mp.learn.gs.business.spi.service;

import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.spi.value.GroupCriteria;
import com.mp.learn.gs.business.spi.value.GroupState;

/**
 * A DAO for Group states.
 */
public interface GroupDao {

  /**
   * Returns states of all the Groups that meet the given {@link GroupCriteria}.
   */
  Iterable<GroupState> query(GroupCriteria criteria);

  /**
   * Saves the given state.
   */
  void save(GroupState state);

  /**
   * Saves all the given states.
   */
  void saveBatch(Iterable<GroupState> states);

  /**
   * Loads a state of the given id.
   * <p>
   * It is illegal to call this method with an id of an inexistent Group.
   */
  GroupState load(GroupId groupId);
}
