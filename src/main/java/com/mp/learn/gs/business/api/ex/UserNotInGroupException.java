package com.mp.learn.gs.business.api.ex;

/**
 * A {@link GroupBusinessLogicContingencyException contingency} thrown when a user tries to leave a
 * group which they are not a member of.
 */
public class UserNotInGroupException extends GroupBusinessLogicContingencyException {

  private static final long serialVersionUID = 3682335936036957619L;
}
