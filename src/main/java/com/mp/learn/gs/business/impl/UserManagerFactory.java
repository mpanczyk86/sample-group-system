package com.mp.learn.gs.business.impl;

import javax.inject.Inject;

import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.impl.acl.UserAuthorizerFactory;
import com.mp.learn.gs.business.impl.asm.UserInfoAssembler;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.service.Transactor;
import com.mp.learn.gs.business.spi.service.UserDao;

public class UserManagerFactory {

  private final UserDao userDao;
  private final UserInfoAssembler userInfoAssembler;
  private final GroupDao groupDao;
  private final UserAuthorizerFactory userAuthorizerFactory;
  private final Transactor transactor;

  @Inject
  public UserManagerFactory(
      UserDao userDao, 
      GroupDao groupDao,
      Transactor transactor,
      UserInfoAssembler userInfoAssembler,
      UserAuthorizerFactory userAuthorizerFactory) {
    this.userDao = userDao;
    this.groupDao = groupDao;
    this.transactor = transactor;
    this.userInfoAssembler = userInfoAssembler;
    this.userAuthorizerFactory = userAuthorizerFactory;
  }
  
  public UserManagerImpl create(UserId userId) {
    return new UserManagerImpl(userId, userDao, groupDao, transactor, userAuthorizerFactory,
        userInfoAssembler);
  }
}