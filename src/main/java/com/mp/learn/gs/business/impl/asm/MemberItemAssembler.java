package com.mp.learn.gs.business.impl.asm;

import javax.inject.Inject;

import com.mp.learn.gs.business.api.value.MemberItem;
import com.mp.learn.gs.business.spi.value.MembershipState;
import com.mp.learn.gs.business.spi.value.UserState;

public class MemberItemAssembler {

  private final UserItemAssembler userItemAssembler;

  @Inject
  public MemberItemAssembler(UserItemAssembler userItemAssembler) {
    this.userItemAssembler = userItemAssembler;
  }
  
  public MemberItem apply(MembershipState state, UserState userState) {
    return MemberItem.create(userItemAssembler.apply(userState), state.joinInstant());    
  }
}
