package com.mp.learn.gs.business.api.value;

import java.util.UUID;

/**
 * An {@link UUID}-based identifier meant for external representation.
 */
public abstract class ExternalId {

	private final UUID uuid;
	
	protected ExternalId(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * Returns the external representation.
	 */
	public UUID asUuid() {
		return uuid;
	}
	
	@Override
	public int hashCode() {
	  return uuid.hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
	  if (!(other instanceof ExternalId)) {
	    return false;
	  }
	  ExternalId otherExternalId = (ExternalId) other;
	  return uuid.equals(otherExternalId.uuid);
	}
	
	@Override
	public String toString() {
	  return uuid.toString();
	}
}
