package com.mp.learn.gs.business.api.value;

import java.time.Instant;
import java.util.List;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;

/**
 * Details of a Group.
 */
@AutoValue
public abstract class GroupInfo {

  /**
   * A name.
   */
  public abstract String name();

  /**
   * An {@link Instant} at which the Group has been created.
   */
  public abstract Instant creationInstant();

  /**
   * A size limit.
   */
  public abstract int limit();
  
  /**
   * Members.
   */
  public abstract List<MemberItem> members();
  
  /**
   * Creates an instance.
   */
  public static GroupInfo create(String name, Instant creationInstant, int limit, 
      Iterable<MemberItem> members) {
		return new AutoValue_GroupInfo(name, creationInstant, limit, ImmutableList.copyOf(members));
	}
}
