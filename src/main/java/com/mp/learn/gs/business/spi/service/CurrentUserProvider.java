package com.mp.learn.gs.business.spi.service;

import com.mp.learn.gs.business.api.value.UserId;

/**
 * A provider of the currently authenticated User.
 */
public interface CurrentUserProvider {

  /**
   * Returns a declared id of the currently authenticated User.
   * <p>
   * The id is "declared" in a sense that no guarantee is given on the existence of the actual User
   * that has been authenticated.
   * <p>
   * It is illegal to call this method when no User has been authenticated.
   */
  UserId getCurrentUserId();
}
