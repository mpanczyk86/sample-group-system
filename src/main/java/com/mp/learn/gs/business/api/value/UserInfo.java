package com.mp.learn.gs.business.api.value;

import java.time.Instant;

import com.google.auto.value.AutoValue;

/**
 * Details of a User.
 */
@AutoValue
public abstract class UserInfo {

  /**
   * A name.
   */
  public abstract String name();
  
  /**
   * An {@link Instant} at which the User has signed up.
   */
  public abstract Instant signUpInstant();

  /**
   * Creates an instance.
   */
  public static UserInfo create(String name, Instant signUpInstant) {
    return new AutoValue_UserInfo(name, signUpInstant);
  }
}
