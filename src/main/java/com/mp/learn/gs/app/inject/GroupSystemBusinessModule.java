package com.mp.learn.gs.app.inject;

import com.google.inject.AbstractModule;
import com.mp.learn.gs.business.api.service.GroupService;
import com.mp.learn.gs.business.api.service.UserService;
import com.mp.learn.gs.business.impl.GroupServiceImpl;
import com.mp.learn.gs.business.impl.UserServiceImpl;

public class GroupSystemBusinessModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(GroupService.class).to(GroupServiceImpl.class);
    bind(UserService.class).to(UserServiceImpl.class);
  }
}
