package com.mp.learn.it.spi;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.service.CurrentUserProvider;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.service.Transactor;
import com.mp.learn.gs.business.spi.service.UserDao;

public class GroupSystemTestSpi {

  private final SettableAuthenticator settableAuthenticator;
  private final SettableClock settableClock;

  public GroupSystemTestSpi() {
    this.settableAuthenticator = new SettableAuthenticator();
    this.settableClock = new SettableClock();
  }

  public void setCurrentUser(UserId userId) {
    settableAuthenticator.set(userId);
  }

  public Instant getCurrentClockInstant() {
    return settableClock.instant();
  }
  
  public void advanceClockBy(Duration duration) {
    settableClock.set(settableClock.instant().plus(duration));
  }

  public Module getModule() {
    return new AbstractModule() {

      @Override
      protected void configure() {
        bind(GroupDao.class).toInstance(new InMemoryGroupDao());
        bind(UserDao.class).toInstance(new InMemoryUserDao());
        bind(Transactor.class).toInstance(new LockingTransactor());
        bind(CurrentUserProvider.class).toInstance(settableAuthenticator);
        bind(Clock.class).toInstance(settableClock);
      }
    };
  }
}
