package com.mp.learn.it.spi;

import com.mp.learn.gs.business.spi.service.Transactor;

public class LockingTransactor implements Transactor {

  private final Object lock;
  
  public LockingTransactor() {
    this.lock = new Object();
  }
  
  @Override
  public <T> T transact(Work<T> work) {
    synchronized (lock) {
      return work.perform();
    }
  }

  @Override
  public void transact(VoidWork voidWork) {
    synchronized (lock) {
      voidWork.perform();
    }
  }
}
