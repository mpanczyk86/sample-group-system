package com.mp.learn.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mp.learn.gs.app.inject.GroupSystemBusinessModule;
import com.mp.learn.gs.business.api.ex.GroupLimitReachedException;
import com.mp.learn.gs.business.api.ex.UserAlreadyInGroupException;
import com.mp.learn.gs.business.api.ex.UserNotAuthorizedException;
import com.mp.learn.gs.business.api.ex.UserNotInGroupException;
import com.mp.learn.gs.business.api.service.GroupService;
import com.mp.learn.gs.business.api.service.UserService;
import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.api.value.GroupInfo;
import com.mp.learn.gs.business.api.value.GroupItem;
import com.mp.learn.gs.business.api.value.MemberItem;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.api.value.UserInfo;
import com.mp.learn.gs.business.api.value.UserItem;
import com.mp.learn.it.spi.GroupSystemTestSpi;
import com.mp.learn.it.spi.InMemoryDaoFault;
import com.mp.learn.it.spi.SettableAuthenticatorFault;

public class GroupSystemTest {

  private static final String USER_NAME = "user name";
  private static final String ANOTHER_USER_NAME = "another user name";
  private static final String GROUP_NAME = "group name";
  private static final int GROUP_LIMIT = 20;
  private static final int TRANSACTIONALITY_THRESHOLD = 500;

  private GroupSystemTestSpi testSpi;

  private GroupService groupService;
  private UserService userService;
  
  @Before
  public void initialize() {
    this.testSpi = new GroupSystemTestSpi();

    Injector injector = Guice.createInjector(new GroupSystemBusinessModule(), testSpi.getModule());
    this.groupService = injector.getInstance(GroupService.class);
    this.userService = injector.getInstance(UserService.class);
  }

  @Test
  public void showsNewlyCreatedUserInfo() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    UserInfo userInfo = userService.manage(userId).show();
    assertThat(userInfo).isEqualTo(UserInfo.create(USER_NAME, testSpi.getCurrentClockInstant()));
  }
  
  @Test
  public void listsNewlyCreatedGroupInfo() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);
    
    List<GroupItem> groupItems = groupService.list();
    assertThat(groupItems).containsExactly(GroupItem.create(groupId, GROUP_NAME, 1));
  }
  
  @Test
  public void showsNewlyCreatedGroupInfo() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);
    
    GroupInfo groupInfo = groupService.manage(groupId).show();
    assertThat(groupInfo).isEqualTo(GroupInfo.create(
        GROUP_NAME, 
        testSpi.getCurrentClockInstant(),
        GROUP_LIMIT,
        Arrays.asList(MemberItem.create(
            UserItem.create(userId, USER_NAME),
            testSpi.getCurrentClockInstant()))));  
  }

  @Test
  public void forbidsToJoinTheSameGroupAgain() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);

    assertThat(catchThrowable(() -> groupService.manage(groupId).join()))
        .isInstanceOf(UserAlreadyInGroupException.class);
  }

  @Test
  public void forbidsToRemoveUserFromNonJoinedGroup() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);

    UserId anotherUserId = userService.create(ANOTHER_USER_NAME);
    testSpi.setCurrentUser(anotherUserId);
    
    assertThat(catchThrowable(() -> groupService.manage(groupId).leave()))
        .isInstanceOf(UserNotInGroupException.class);
  }
  
  @Test
  public void showsNewlyJoinedMemberWithinGroupInfo() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);

    UserId anotherUserId = userService.create(ANOTHER_USER_NAME);
    testSpi.setCurrentUser(anotherUserId);
    
    groupService.manage(groupId).join();
    
    GroupInfo groupInfo = groupService.manage(groupId).show();
    assertThat(groupInfo.members())
        .extracting(memberItem -> memberItem.user().id())
        .containsExactly(userId, anotherUserId);
  }
  
  @Test
  public void forbidsToJoinGroupAfterReachingItsSizeLimit() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);

    for (int i = 1; i < GROUP_LIMIT; ++i) {
      UserId anotherUserId = userService.create(ANOTHER_USER_NAME);
      testSpi.setCurrentUser(anotherUserId);

      groupService.manage(groupId).join();
    }

    UserId overLimitUserId = userService.create(USER_NAME);
    testSpi.setCurrentUser(overLimitUserId);

    assertThat(catchThrowable(() -> groupService.manage(groupId).join()))
        .isInstanceOf(GroupLimitReachedException.class);
  }

  @Test
  public void removesMemberFromGroupAfterLeaving() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);
    groupService.manage(groupId).leave();
    
    GroupInfo groupInfo = groupService.manage(groupId).show();
    assertThat(groupInfo.members()).isEmpty();
  }

  @Test
  public void propagatesDaoFaultWhenTryingToShowDeletedUser() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    userService.manage(userId).delete();
    
    UserId anotherUserId = userService.create(ANOTHER_USER_NAME);
    testSpi.setCurrentUser(anotherUserId);

    assertThat(catchThrowable(() -> userService.manage(userId).show()))
        .isInstanceOf(InMemoryDaoFault.class);
  }
  
  @Test
  public void forbidsToDeleteUserOtherThanSelf() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    UserId anotherUserId = userService.create(ANOTHER_USER_NAME);
    testSpi.setCurrentUser(anotherUserId);
    
    assertThat(catchThrowable(() -> userService.manage(userId).delete()))
        .isInstanceOf(UserNotAuthorizedException.class);
  }
  
  @Test
  public void propagatesDaoFaultWhenDeletedUserIsSetAsCurrentUser() {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    userService.manage(userId).delete();
    
    assertThat(catchThrowable(() -> groupService.create(GROUP_NAME, GROUP_LIMIT)))
        .isInstanceOf(InMemoryDaoFault.class);
  }
    
  @Test
  public void propagatesAuthenticationFaultWhenNoCurrentUserIsSet() {
    assertThat(catchThrowable(() -> groupService.create(GROUP_NAME, GROUP_LIMIT)))
        .isInstanceOf(SettableAuthenticatorFault.class);
  }
  
  @Test
  public void obeysTransactionality() throws Exception {
    UserId userId = userService.create(USER_NAME);
    testSpi.setCurrentUser(userId);
    
    GroupId groupId = groupService.create(GROUP_NAME, GROUP_LIMIT);

    Collection<Thread> threads = new ArrayList<>();
    for (int i = GROUP_LIMIT; i < TRANSACTIONALITY_THRESHOLD; ++i) {
      Thread thread = new Thread(() -> groupService.manage(groupId).increaseLimit());
      thread.start();
      threads.add(thread);
    }
    for (Thread thread : threads) {
      thread.join();
    }
    
    assertThat(groupService.manage(groupId).show().limit()).isEqualTo(TRANSACTIONALITY_THRESHOLD);
  }
}
